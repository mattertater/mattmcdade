use             hex       r   g   b

background      #343538   052,053,056
deep background #000000   000,000,000
text            #cccccc   204,204,204
red special     #a85751   168,087,081
links           #54a9d3   085,169,211
link hover      #73b8db   115,184,219
yellow special  #d4c685   212,198,133